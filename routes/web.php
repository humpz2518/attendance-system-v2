<?php



Route::get('/', function () {
    return view('welcome');
})->name('welcome')->middleware('guest');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/sign_in', 'DataTableController@sign_in')->name('sign-in');
Route::get('/home_V2', 'HomeController@index_V2')->name('home-V2');
Route::get('/show_add_event', 'HomeController@show_add_event')->name('show-add-event');
Route::get('/show_view_events', 'HomeController@show_view_events')->name('show-view-events');
Route::get('/add_event', 'HomeController@add_event')->name('add-event');

Route::get('/show_add_school', 'HomeController@show_add_school')->name('show-add-school');
Route::get('/add_school', 'HomeController@add_school')->name('add-school');

Route::get('/show_add_attendee', 'HomeController@show_add_attendee')->name('show-add-attendee');
Route::get('/show_add_attendee_V2', 'HomeController@show_add_attendee_V2')->name('show-add-attendee-V2');
Route::get('/show_view_attendees', 'HomeController@show_view_attendees')->name('show-view-attendees');
Route::get('/show_view_attendees_V2', 'HomeController@show_view_attendees_V2')->name('show-view-attendees-V2');
Route::post('/add_attendee', 'HomeController@add_attendee')->name('add-attendee');

//datatables
Route::get('/events/datatable','DataTableController@view_events_Datatable')->name('events-value');
Route::get('/attendees/datatable','DataTableController@view_attendees_Datatable')->name('attendees-value');

Route::get('/download/attendees-csv','HomeController@View_Attendees_CSV')->name('attendees-csv');
