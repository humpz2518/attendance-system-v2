<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendees', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id')->nullable();
            $table->unsignedInteger('school_id')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('course');
            $table->string('year');
            $table->string('school');
            $table->string('ID_no');
            $table->boolean('morning_sign_in')->default(0);
            $table->boolean('second_morning_sign_in')->default(0);
            $table->boolean('third_morning_sign_in')->default(0);
            $table->boolean('afternoon_sign_in')->default(0);
            $table->boolean('second_afternoon_sign_in')->default(0);
            $table->boolean('third_afternoon_sign_in')->default(0);
            $table->boolean('evening_sign_in')->default(0);
            $table->timestamps();
            $table->foreign('event_id')->references('id')->on('events');
            $table->foreign('school_id')->references('id')->on('schools');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendees');
    }
}
