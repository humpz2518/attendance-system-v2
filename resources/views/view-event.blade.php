@extends('layouts.master')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong>View Event</strong>
            </div>
            <div class="card-body card-block">
                <table id="view-events-table" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Date</th>
                    </tr>
                    </thead>

                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function (){
            data_table.init();
        });

        var data_table = {
            data : {},
            init : function() {
                var events_table = $('#view-events-table');
                events_table.DataTable({
                    serverSide: true,
                    autoWidth: true,
                    ajax: {
                        "url": '/events/datatable'
                    },
                    columns: [
                        {data: 'id', name: 'id',orderable: false},
                        {data: 'name', name: 'name',orderable: true},
                        {data: 'date', name: 'date',orderable: false},
                    ],

                    "dom": 'lCfrtip',
                    "order": [],
                    "colVis": {
                        "buttonText": "Columns",
                        "overlayFade": 0,
                        "align": "right"
                    },
                    "language": {
                        "lengthMenu": '_MENU_ entries per page',
                        "search": '<i class="fa fa-search" style=""></i>',
                        "paginate": {
                            "previous": '<i class="fa fa-angle-left"></i>',
                            "next": '<i class="fa fa-angle-right"></i>'
                        }
                    }
                });
            }
        };
    </script>
@stop