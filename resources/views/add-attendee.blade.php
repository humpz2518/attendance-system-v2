@extends('layouts.master')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong>Choose Event</strong>
            </div>
            <div class="card-body card-block">
                <form method="get" class="form-horizontal" action="{{route('show-add-attendee-V2')}}">
                    <div class="form-group">
                            <select class="form-control" name="events">
                                @foreach($events as $event)
                                <option>{{$event->name}}</option>
                                @endforeach
                            </select>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary btn-lg form-control" id="generate">Next</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

