<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Attendance System</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">
</head>

<body id="top">
<section class="hero">
    <section class="navigation">
        <header>
            <div class="header-content">
                <div class="logo"><a href="{{route('welcome')}}"><img src="{{asset('logo.png')}}" alt="Attendance System"></a></div>
                <div class="header-nav">
                    <nav>
                        <ul class="member-actions">
                            <li><a href="{{route('login')}}" class="login">Log in</a></li>
                            <li><a href="{{route('register')}}" class="btn-white btn-small">Sign up</a></li>
                        </ul>
                    </nav>
                </div>
                {{--<div class="navicon">--}}
                    {{--<a class="nav-toggle" href="#"><span></span></a>--}}
                {{--</div>--}}
            </div>
        </header>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="hero-content text-center">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>
