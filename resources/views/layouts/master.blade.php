<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Attendance System Admin</title>
    <meta name="description" content="Attendance System Admin">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" type="text/css" href="{{asset('css/daterangepicker.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.dataTables.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
    {{--<link rel="stylesheet" href="{{asset('css/flag-icon.min.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('css/cs-skin-elastic.css')}}">--}}
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="{{asset('scss/style.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    @yield('stylesheet')

</head>
<body>


<!-- Left Panel -->

<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="{{route('home')}}"><img src="{{asset('logo.png')}}" alt="Logo"></a>
            <a class="navbar-brand hidden" href="{{route('home')}}"><img src="{{asset('logo2.png')}}" alt="Logo"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{route('home')}}"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                </li>
                <h3 class="menu-title">Events</h3><!-- /.menu-title -->
                <li>
                    <a href="{{route('show-add-event')}}"> <i class="menu-icon fa fa-calendar"></i>Add Event</a>
                </li>
                <li>
                    <a href="{{route('show-view-events')}}"> <i class="menu-icon fa fa-calendar-o"></i>View Events</a>
                </li>

                <h3 class="menu-title">Schools</h3><!-- /.menu-title -->
                <li>
                    <a href="{{route('show-add-school')}}"> <i class="menu-icon fa fa-calendar"></i>Add School</a>
                </li>

                <h3 class="menu-title">Attendees</h3><!-- /.menu-title -->

                <li>
                    <a href="{{route('show-add-attendee')}}"> <i class="menu-icon fa fa-user"></i>Add Attendee</a>
                </li>

                <li>
                    <a href="{{route('show-view-attendees')}}"> <i class="menu-icon fa fa-users"></i>View Attendees</a>
                </li>

                {{--<li class="menu-item-has-children dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Icons</a>--}}
                    {{--<ul class="sub-menu children dropdown-menu">--}}
                        {{--<li><i class="menu-icon fa fa-fort-awesome"></i><a href="font-fontawesome.html">Font Awesome</a></li>--}}
                        {{--<li><i class="menu-icon ti-themify-logo"></i><a href="font-themify.html">Themefy Icons</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="widgets.html"> <i class="menu-icon ti-email"></i>Widgets </a>--}}
                {{--</li>--}}
                {{--<li class="menu-item-has-children dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bar-chart"></i>Charts</a>--}}
                    {{--<ul class="sub-menu children dropdown-menu">--}}
                        {{--<li><i class="menu-icon fa fa-line-chart"></i><a href="charts-chartjs.html">Chart JS</a></li>--}}
                        {{--<li><i class="menu-icon fa fa-area-chart"></i><a href="charts-flot.html">Flot Chart</a></li>--}}
                        {{--<li><i class="menu-icon fa fa-pie-chart"></i><a href="charts-peity.html">Peity Chart</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}

                {{--<li class="menu-item-has-children dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Maps</a>--}}
                    {{--<ul class="sub-menu children dropdown-menu">--}}
                        {{--<li><i class="menu-icon fa fa-map-o"></i><a href="maps-gmap.html">Google Maps</a></li>--}}
                        {{--<li><i class="menu-icon fa fa-street-view"></i><a href="maps-vector.html">Vector Maps</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<h3 class="menu-title">Extras</h3><!-- /.menu-title -->--}}
                {{--<li class="menu-item-has-children dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Pages</a>--}}
                    {{--<ul class="sub-menu children dropdown-menu">--}}
                        {{--<li><i class="menu-icon fa fa-sign-in"></i><a href="page-login.html">Login</a></li>--}}
                        {{--<li><i class="menu-icon fa fa-sign-in"></i><a href="page-register.html">Register</a></li>--}}
                        {{--<li><i class="menu-icon fa fa-paper-plane"></i><a href="pages-forget.html">Forget Pass</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->

<!-- Left Panel -->

<!-- Right Panel -->

<div id="right-panel" class="right-panel">


    <!-- Header-->
    <header id="header" class="header">

        <div class="header-menu">


            <div class="col-sm-12">
                <div class="user-area dropdown float-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="user-avatar rounded-circle" src="{{asset('admin.jpg')}}" alt="User Avatar" style="width: 50px;">
                    </a>

                    <div class="user-menu dropdown-menu">
                        <a class="nav-link" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>

                <div class="language-select dropdown" id="language-select">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
                        <i class="flag-icon flag-icon-us"></i>
                    </a>
                </div>

            </div>
        </div>

    </header><!-- /header -->
    <!-- Header-->

    @yield('content')

    <footer>
        <div class="footer-copyright text-center py-3">Created By:
            <h5>Humphrey Aljas</h5>
        </div>
    </footer>



</div><!-- /#right-panel --><!-- /#right-panel -->


<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/daterangepicker.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins.js')}}"></script>
{{--<script src="{{asset('js/main.js')}}"></script>--}}
{{--<script src="{{asset('js/lib/chart-js/Chart.bundle.js')}}"></script>--}}
{{--<script src="{{asset('js/dashboard.js')}}"></script>--}}
{{--<script src="{{asset('js/widgets.js')}}"></script>--}}
{{--<script src="{{asset('js/lib/vector-map/jquery.vmap.js')}}"></script>--}}
{{--<script src="{{asset('js/lib/vector-map/jquery.vmap.min.js')}}"></script>--}}
{{--<script src="{{asset('js/lib/vector-map/jquery.vmap.sampledata.js')}}"></script>--}}
{{--<script src="{{asset('js/lib/vector-map/country/jquery.vmap.world.js')}}"></script>--}}
<script src="{{asset('js/sweetalert.min.js')}}"></script>


@yield('script')
</body>
</html>
