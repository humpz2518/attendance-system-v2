@extends('layouts.master')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong>Add School</strong>
            </div>
            <div class="card-body card-block">
                <form method="get" class="form-horizontal">
                        <div class="row form-group">
                            <div class="col col-md-2"><label for="text-input" class=" form-control-label">Name of School</label></div>
                            <div class="col-12 col-md-10"><input type="text" id="school_name" name="school_name" placeholder="Enter name of school" class="form-control" required></div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success btn-lg form-control" type="button" id="submit_add_school" data-route="{{route('add-school')}}">Submit</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#submit_add_school').click(function() {
                var url = $(this).data('route');
                $.ajax({
                    url: url,
                    method: 'GET',
                    data: {
                        name: $('#school_name').val()
                    },
                    dataType: 'json',
                    success: function(data){
                        if(data.type == 200){
                            swal("Success!", data.message, "success");
                        }
                        else{
                            swal("Failed!", data.message, "error");
                        }
                    }
                });
            });
        });
    </script>
@stop
