@extends('layouts.master')

@section('content')
    <div class="col-sm-12 mb-4">
        <div class="card-group">
            <div class="card col-lg-2 col-md-6 no-padding" style="background: #808080;">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="fa fa-address-card-o  text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$events}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Total No. of Events</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body" style="background: #808080;">
                    <div class="h1 text-right mb-4">
                        <i class="fa fa-users text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$all}}</span>
                    </div>
                    <small class="text-light text-uppercase font-weight-bold">Total Attendees</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body" style="background: #808080;">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$morning_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Total Morning Signed in Attendees</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body" style="background: #808080;">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$afternoon_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Total Afternoon Signed in Attendees</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body" style="background: #808080;">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$evening_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Total Evening Signed in Attendees</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
        </div>

        <div class="card-group" style="margin-top: 10px;">
            <div class="card col-lg-2 col-md-6 no-padding bg-flat-color-1">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="fa fa-users  text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$acsat_count}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Total Attendees for Asian College</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-5">
                    <div class="h1 text-right mb-4">
                        <i class="fa fa-users text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$spud_count}}</span>
                    </div>
                    <small class="text-light text-uppercase font-weight-bold">Total Attendees for St. Paul University Dumaguete</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-3">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$norsu_count}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Total Attendees for Negros Oriental State University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body" style="background: #800000;">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$fu_count}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Total Attendees for Foundation University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-4">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$su_count}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Total Attendees for Silliman University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
        </div>

        <div class="card-group" style="margin-top: 10px;">
            <div class="card col-lg-2 col-md-6 no-padding bg-flat-color-1">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="fa fa-check-square-o  text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$acsat_morning_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">First Morning signed in Asian College</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-5">
                    <div class="h1 text-right mb-4">
                        <i class="fa fa-check-square-o text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$spud_morning_sign_in}}</span>
                    </div>
                    <small class="text-light text-uppercase font-weight-bold">First Morning signed in St. Paul University Dumaguete</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-3">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$norsu_morning_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">First Morning signed in Negros Oriental State University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body" style="background: #800000;">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$fu_morning_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">First Morning signed in Foundation University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-4">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$su_morning_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">First Morning signed in Silliman University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
        </div>

        <div class="card-group" style="margin-top: 10px;">
            <div class="card col-lg-2 col-md-6 no-padding bg-flat-color-1">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="fa fa-check-square-o  text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$acsat_second_morning_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Second Morning signed in Asian College</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-5">
                    <div class="h1 text-right mb-4">
                        <i class="fa fa-check-square-o text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$spud_second_morning_sign_in}}</span>
                    </div>
                    <small class="text-light text-uppercase font-weight-bold">Second Morning signed in St. Paul University Dumaguete</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-3">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$norsu_second_morning_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Second Morning signed in Negros Oriental State University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body" style="background: #800000;">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$fu_second_morning_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Second Morning signed in Foundation University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-4">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$su_second_morning_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Second Morning signed in Silliman University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
        </div>

        <div class="card-group" style="margin-top: 10px;">
            <div class="card col-lg-2 col-md-6 no-padding bg-flat-color-1">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="fa fa-check-square-o  text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$acsat_third_morning_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Third Morning signed in Asian College</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-5">
                    <div class="h1 text-right mb-4">
                        <i class="fa fa-check-square-o text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$spud_third_morning_sign_in}}</span>
                    </div>
                    <small class="text-light text-uppercase font-weight-bold">Third Morning signed in St. Paul University Dumaguete</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-3">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$norsu_third_morning_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Third Morning signed in Negros Oriental State University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body" style="background: #800000;">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$fu_third_morning_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Third Morning signed in Foundation University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-4">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$su_third_morning_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Third Morning signed in Silliman University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
        </div>

        <div class="card-group" style="margin-top: 10px;">
            <div class="card col-lg-2 col-md-6 no-padding bg-flat-color-1">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="fa fa-check-square-o  text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$acsat_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">First Afternoon signed in Asian College</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-5">
                    <div class="h1 text-right mb-4">
                        <i class="fa fa-check-square-o text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$spud_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-light text-uppercase font-weight-bold">First Afternoon signed in St. Paul University Dumaguete</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-3">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$norsu_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">First Afternoon signed in Negros Oriental State University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body" style="background: #800000;">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$fu_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">First Afternoon signed in Foundation University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-4">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$su_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">First Afternoon signed in Silliman University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
        </div>

        <div class="card-group" style="margin-top: 10px;">
            <div class="card col-lg-2 col-md-6 no-padding bg-flat-color-1">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="fa fa-check-square-o  text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$acsat_second_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Second Afternoon signed in Asian College</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-5">
                    <div class="h1 text-right mb-4">
                        <i class="fa fa-check-square-o text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$spud_second_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-light text-uppercase font-weight-bold">Second Afternoon signed in St. Paul University Dumaguete</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-3">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$norsu_second_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Second Afternoon signed in Negros Oriental State University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body" style="background: #800000;">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$fu_second_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Second Afternoon signed in Foundation University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-4">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$su_second_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Second Afternoon signed in Silliman University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
        </div>

        <div class="card-group" style="margin-top: 10px;">
            <div class="card col-lg-2 col-md-6 no-padding bg-flat-color-1">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="fa fa-check-square-o  text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$acsat_third_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Third Afternoon signed in Asian College</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-5">
                    <div class="h1 text-right mb-4">
                        <i class="fa fa-check-square-o text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$spud_third_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-light text-uppercase font-weight-bold">Third Afternoon signed in St. Paul University Dumaguete</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-3">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$norsu_third_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Third Afternoon signed in Negros Oriental State University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body" style="background: #800000;">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$fu_third_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Third Afternoon signed in Foundation University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-4">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$su_third_afternoon_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Third Afternoon signed in Silliman University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
        </div>

        <div class="card-group" style="margin-top: 10px;">
            <div class="card col-lg-2 col-md-6 no-padding bg-flat-color-1">
                <div class="card-body">
                    <div class="h1 text-muted text-right mb-4">
                        <i class="fa fa-check-square-o  text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$acsat_evening_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Evening signed in Asian College</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-5">
                    <div class="h1 text-right mb-4">
                        <i class="fa fa-check-square-o text-light"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$spud_evening_sign_in}}</span>
                    </div>
                    <small class="text-light text-uppercase font-weight-bold">Evening signed in St. Paul University Dumaguete</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-3">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$norsu_evening_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Evening signed in Negros Oriental State University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body" style="background: #800000;">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$fu_evening_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Evening signed in Foundation University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
            <div class="card col-lg-2 col-md-6 no-padding no-shadow">
                <div class="card-body bg-flat-color-4">
                    <div class="h1 text-right text-light mb-4">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                    <div class="h4 mb-0 text-light">
                        <span class="count">{{$su_evening_sign_in}}</span>
                    </div>
                    <small class="text-uppercase font-weight-bold text-light">Evening signed in Silliman University</small>
                    <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                </div>
            </div>
        </div>
    </div>



@endsection
