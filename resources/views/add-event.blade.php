@extends('layouts.master')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong>Add Event</strong>
            </div>
            <div class="card-body card-block">
                <form method="get" class="form-horizontal" id="add_event">
                    @if(isset($_GET['event_name']))
                        <div class="row form-group">
                            <div class="col col-md-2"><label for="text-input" class=" form-control-label">Name of Event</label></div>
                            <div class="col-12 col-md-10"><input type="text" id="event_name" name="event_name" placeholder="Enter name of event" value="{{$_GET['event_name']}}" class="form-control" required></div>
                        </div>
                    @else
                        <div class="row form-group">
                            <div class="col col-md-2"><label for="text-input" class=" form-control-label">Name of Event</label></div>
                            <div class="col-12 col-md-10"><input type="text" id="event_name" name="event_name" placeholder="Enter name of event" class="form-control" required></div>
                        </div>
                    @endif
                    @if(isset($_GET['event_name']))
                        <div class="row form-group">
                            <div class="col col-md-2"><label for="text-input" class=" form-control-label">Number Of Days</label></div>
                            <div class="col-12 col-md-10"><input type="text" id="num_days" name="num_days" placeholder="Enter number of days" value="{{$_GET['num_days']}}" class="form-control" required></div>
                        </div>
                    @else
                        <div class="row form-group">
                            <div class="col col-md-2"><label for="text-input" class=" form-control-label">Number Of Days</label></div>
                            <div class="col-12 col-md-10"><input type="text" id="num_days" name="num_days" placeholder="Enter number of days" class="form-control" required></div>
                        </div>
                    @endif
                    <div class="form-group">
                        <button class="btn btn-primary btn-lg form-control" id="generate">Generate</button>
                    </div>

                    @if(isset($_GET['num_days']))
                        @for($x = 1;$x<=$_GET['num_days'];$x++)
                        <div class="form-group">
                            <label class=" form-control-label" id="event_name_label{{$x}}">{{$_GET['event_name']}} Day{{$x}}</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type='text' autocomplete="off" class="form-control" id="start_date{{$x}}" value="{{Carbon\Carbon::today()->addDay($x)->format('m/d/Y')}}"/>
                            </div>
                        </div>
                        @endfor
                        <div class="form-group">
                                <button class="btn btn-success btn-lg form-control" type="button" id="submit_add_event" data-route="{{route('add-event')}}">Submit</button>
                            </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }

        for(var x=1;x<=getUrlVars()['num_days'];x++){
            $('#start_date'+x).daterangepicker({
                singleDatePicker: true
            });
        }

        $('#submit_add_event').click(function() {
            var url = $(this).data('route');
            var arr=[];
            var arr2=[];
            var days = $('#num_days').val();
            for(var x=1;x<=days;x++){
                arr.push($('#start_date'+x).val());
                arr2.push($('#event_name_label'+x).text());
            }
            $.ajax({
                url: url,
                method: 'GET',
                data: {
                    days: days,
                    dates: arr,
                    names: arr2
                },
                dataType: 'json',
                success: function(data){
                    swal("Success!", data.message, "success");
                },
                error: function (data) {
                    swal("An Error Occurred!", "Failed to add event. Event may already been registered.", "error");
                }
            });
        });
    });
    </script>
@stop
