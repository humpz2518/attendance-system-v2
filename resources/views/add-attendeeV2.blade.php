@extends('layouts.master')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong>Add Attendee</strong>
            </div>
            <div class="card-body card-block">
                <form method="POST" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="text-input" class=" form-control-label">First Name</label></div>
                        <div class="col-12 col-md-10"><input type="text" id="first_name" name="first_name" placeholder="Enter Firstname" class="form-control" required></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="text-input" class=" form-control-label">Last Name</label></div>
                        <div class="col-12 col-md-10"><input type="text" id="last_name" name="last_name" placeholder="Enter Lastname" class="form-control" required></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="text-input" class=" form-control-label">Course</label></div>
                        <div class="col-12 col-md-10">
                            <select class="form-control" name="course" id="course">
                                    <option>BSA</option>
                                    <option>BSMA</option>
                                    <option>BSAT</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="text-input" class=" form-control-label">Year</label></div>
                        <div class="col-12 col-md-10">
                            <select class="form-control" name="year" id="year">
                                <option>First Year</option>
                                <option>Second Year</option>
                                <option>Third Year</option>
                                <option>Fourth Year</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="text-input" class=" form-control-label">School</label></div>
                        <div class="col-12 col-md-10">
                            <select class="form-control" name="school" id="school">
                                    @foreach($schools as $school)
                                        <option>{{$school->name}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-2"><label for="text-input" class=" form-control-label">ID</label></div>
                        <div class="col-12 col-md-10"><input type="text" id="ID" name="ID" placeholder="Enter ID Number" class="form-control" required></div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-5 offset-md-5">
                            <button class="btn btn-primary btn-lg" type="button" id="submit_add_attendee" data-route="{{route('add-attendee')}}" data-event="{{$_GET['events']}}">Submit</button>
                            <a href="{{route('show-add-attendee')}}"><button class="btn btn-danger btn-lg" type="button" id="submit_add_event">Back</button></a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#submit_add_attendee').click(function(){
                var url = $(this).data('route');
                var event = $(this).data('event');
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        _token: '{{csrf_token()}}',
                        event: event,
                        first_name: $('#first_name').val(),
                        last_name: $('#last_name').val(),
                        course: $('#course').val(),
                        year: $('#year').val(),
                        school: $('#school').val(),
                        ID: $('#ID').val()
                    },
                    dataType: 'json',
                    success: function(data){
                        if(data.type == 400){
                            swal("Error!", data.message, "error");
                        }
                        else if(data.type == 200){
                            swal("Success!", data.message, "success");
                        }
                    }
                });
            });
        });
    </script>
@stop
