@extends('layouts.master')
@section('content')
    <div class="col-lg-12">
        <div class="card" id="card" data-value="{{Session::get('event')}}" data-school="{{Session::get('school')}}">
            <div class="card-header">
                <strong>View Attendees</strong>
                <form method="GET" action="{{route('attendees-csv')}}" class="d-inline pull-right">
                    <input type="hidden" name="school" value="{{$_GET['schools']}}">
                    <select name="type" style="width: auto; margin-right: 5px;">
                        <option>All</option>
                        <option>First Morning Sign In</option>
                        <option>Second Morning Sign In</option>
                        <option>Third Morning Sign In</option>
                        <option>First Afternoon Sign In</option>
                        <option>Second Afternoon Sign In</option>
                        <option>Third Afternoon Sign In</option>
                        <option>Evening Sign In</option>
                    </select>
                    <button class="btn btn-success" type="submit">Download CSV</button>
                </form>
            </div>
            <div class="card-body card-block">
                <table id="view-attendees-table" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Course</th>
                        <th>Year</th>
                        <th>School</th>
                        <th>Morning Sign In</th>
                        <th>Second Morning Sign In</th>
                        <th>Third Morning Sign In</th>
                        <th>Afternoon Sign In</th>
                        <th>Second Afternoon Sign In</th>
                        <th>Third Afternoon Sign In</th>
                        <th>Evening Sign In</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function (){
            data_table.init();
        });

        var data_table = {
            data : {},
            init : function() {
                var attendees_table = $('#view-attendees-table');
                var holder = document.getElementById('card');
                var value = holder.getAttribute('data-value');
                var school = holder.getAttribute('data-school');
                attendees_table.DataTable({
                    serverSide: true,
                    autoWidth: true,
                    ajax: {
                        "url": '/attendees/datatable',
                        method: 'GET',
                        data:{
                            event:value,
                            school:school
                        }
                    },
                    columns: [
                        {data: 'id', name: 'id',orderable: false},
                        {data: 'first_name', name: 'first_name',orderable: true},
                        {data: 'last_name', name: 'last_name',orderable: true},
                        {data: 'course', name: 'course',orderable: true},
                        {data: 'year', name: 'year',orderable: true},
                        {data: 'school', name: 'school',orderable: true},
                        {data: 'morning_sign_in', name: 'morning_sign_in',orderable: false},
                        {data: 'second_morning_sign_in', name: '2nd_morning_sign_in',orderable: false},
                        {data: 'third_morning_sign_in', name: '3rd_morning_sign_in',orderable: false},
                        {data: 'afternoon_sign_in', name: 'afternoon_sign_in',orderable: false},
                        {data: 'second_afternoon_sign_in', name: '2nd_afternoon_sign_in',orderable: false},
                        {data: 'third_afternoon_sign_in', name: '3rd_afternoon_sign_in',orderable: false},
                        {data: 'evening_sign_in', name: 'evening_sign_in',orderable: false},
                        {data: 'actions', name: 'actions', orderable: false, searchable: false },
                    ],

                    "dom": 'lCfrtip',
                    "order": [],
                    "colVis": {
                        "buttonText": "Columns",
                        "overlayFade": 0,
                        "align": "right"
                    },
                    "language": {
                        "lengthMenu": '_MENU_ entries per page',
                        "search": '<i class="fa fa-search" style=""></i>',
                        "paginate": {
                            "previous": '<i class="fa fa-angle-left"></i>',
                            "next": '<i class="fa fa-angle-right"></i>'
                        }
                    },
                    fnDrawCallback : function(){
                        $('.attendee').click(function(){
                            var url = $(this).data('route');
                            var row = $(this).closest("tr");
                            var id = row.find('button').data('value');
                            $.ajax({
                                url: url,
                                method: 'GET',
                                data: {
                                    id: id
                                },
                                dataType: 'json',
                                success: function(data){
                                    swal("Success!", data.message, "success");
                                },
                                error: function (data) {
                                    console.log(data);
                                },
                                complete: function(){
                                    $('#view-attendees-table').dataTable().fnDraw();
                                }
                            });
                        });
                    }
                });
            }
        };
    </script>
@stop