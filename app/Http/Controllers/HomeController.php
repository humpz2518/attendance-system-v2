<?php

namespace App\Http\Controllers;

use App\Attendee;
use Illuminate\Http\Request;
use App\Event;
use App\School;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Datatables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('View_Attendees_CSV');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();
        return view('dashboard',['events' => $events]);
    }

    public function index_V2()
    {
        $events = Event::all()->count();
        $event = Event::where('name',$_GET['events'])->first();
        $all = Attendee::where('event_id',$event->id)->count();
        $morning_sign_in = Attendee::where('event_id',$event->id)->where('morning_sign_in','1')->count();
        $afternoon_sign_in = Attendee::where('event_id',$event->id)->where('afternoon_sign_in','1')->count();
        $evening_sign_in = Attendee::where('event_id',$event->id)->where('evening_sign_in','1')->count();
        $acsat = Attendee::where('event_id',$event->id)->where('school_id','1')->count();
        $spud = Attendee::where('event_id',$event->id)->where('school_id','2')->count();
        $norsu = Attendee::where('event_id',$event->id)->where('school_id','3')->count();
        $fu = Attendee::where('event_id',$event->id)->where('school_id','4')->count();
        $su = Attendee::where('event_id',$event->id)->where('school_id','5')->count();
        $acsat_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','1')->where('morning_sign_in','1')->count();
        $acsat_second_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','1')->where('second_morning_sign_in','1')->count();
        $acsat_third_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','1')->where('third_morning_sign_in','1')->count();
        $acsat_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','1')->where('afternoon_sign_in','1')->count();
        $acsat_second_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','1')->where('second_afternoon_sign_in','1')->count();
        $acsat_third_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','1')->where('third_afternoon_sign_in','1')->count();
        $acsat_evening_sign_in = Attendee::where('event_id',$event->id)->where('school_id','1')->where('evening_sign_in','1')->count();
        $spud_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','2')->where('morning_sign_in','1')->count();
        $spud_second_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','2')->where('second_morning_sign_in','1')->count();
        $spud_third_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','2')->where('third_morning_sign_in','1')->count();
        $spud_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','2')->where('afternoon_sign_in','1')->count();
        $spud_second_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','2')->where('second_afternoon_sign_in','1')->count();
        $spud_third_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','2')->where('third_afternoon_sign_in','1')->count();
        $spud_evening_sign_in = Attendee::where('event_id',$event->id)->where('school_id','2')->where('evening_sign_in','1')->count();
        $norsu_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','3')->where('morning_sign_in','1')->count();
        $norsu_second_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','3')->where('second_morning_sign_in','1')->count();
        $norsu_third_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','3')->where('third_morning_sign_in','1')->count();
        $norsu_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','3')->where('afternoon_sign_in','1')->count();
        $norsu_second_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','3')->where('second_afternoon_sign_in','1')->count();
        $norsu_third_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','3')->where('third_afternoon_sign_in','1')->count();
        $norsu_evening_sign_in = Attendee::where('event_id',$event->id)->where('school_id','3')->where('evening_sign_in','1')->count();
        $fu_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','4')->where('morning_sign_in','1')->count();
        $fu_second_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','4')->where('second_morning_sign_in','1')->count();
        $fu_third_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','4')->where('third_morning_sign_in','1')->count();
        $fu_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','4')->where('afternoon_sign_in','1')->count();
        $fu_second_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','4')->where('second_afternoon_sign_in','1')->count();
        $fu_third_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','4')->where('third_afternoon_sign_in','1')->count();
        $fu_evening_sign_in = Attendee::where('event_id',$event->id)->where('school_id','4')->where('evening_sign_in','1')->count();
        $su_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','4')->where('morning_sign_in','1')->count();
        $su_second_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','4')->where('second_morning_sign_in','1')->count();
        $su_third_morning_sign_in = Attendee::where('event_id',$event->id)->where('school_id','4')->where('third_morning_sign_in','1')->count();
        $su_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','4')->where('afternoon_sign_in','1')->count();
        $su_second_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','4')->where('second_afternoon_sign_in','1')->count();
        $su_third_afternoon_sign_in = Attendee::where('event_id',$event->id)->where('school_id','4')->where('third_afternoon_sign_in','1')->count();
        $su_evening_sign_in = Attendee::where('event_id',$event->id)->where('school_id','4')->where('evening_sign_in','1')->count();
        return view('home',['events' => $events,
                            'event' => $event,
                            'all' => $all,
                            'morning_sign_in' => $morning_sign_in,
                            'afternoon_sign_in' => $afternoon_sign_in,
                            'evening_sign_in' => $evening_sign_in,
                            'acsat_count' => $acsat,
                            'spud_count' => $spud,
                            'norsu_count' => $norsu,
                            'fu_count' => $fu,
                            'su_count' => $su,
                            'acsat_morning_sign_in' => $acsat_morning_sign_in,
                            'acsat_second_morning_sign_in' => $acsat_second_morning_sign_in,
                            'acsat_third_morning_sign_in' => $acsat_third_morning_sign_in,
                            'acsat_afternoon_sign_in' => $acsat_afternoon_sign_in,
                            'acsat_second_afternoon_sign_in' => $acsat_second_afternoon_sign_in,
                            'acsat_third_afternoon_sign_in' => $acsat_third_afternoon_sign_in,
                            'acsat_evening_sign_in' => $acsat_evening_sign_in,
                            'spud_morning_sign_in' => $spud_morning_sign_in,
                            'spud_second_morning_sign_in' => $spud_second_morning_sign_in,
                            'spud_third_morning_sign_in' => $spud_third_morning_sign_in,
                            'spud_afternoon_sign_in' => $spud_afternoon_sign_in,
                            'spud_second_afternoon_sign_in' => $spud_second_afternoon_sign_in,
                            'spud_third_afternoon_sign_in' => $spud_third_afternoon_sign_in,
                            'spud_evening_sign_in' => $spud_evening_sign_in,
                            'norsu_morning_sign_in' => $norsu_morning_sign_in,
                            'norsu_second_morning_sign_in' => $norsu_second_morning_sign_in,
                            'norsu_third_morning_sign_in' => $norsu_third_morning_sign_in,
                            'norsu_afternoon_sign_in' => $norsu_afternoon_sign_in,
                            'norsu_second_afternoon_sign_in' => $norsu_second_afternoon_sign_in,
                            'norsu_third_afternoon_sign_in' => $norsu_third_afternoon_sign_in,
                            'norsu_evening_sign_in' => $norsu_evening_sign_in,
                            'fu_morning_sign_in' => $fu_morning_sign_in,
                            'fu_second_morning_sign_in' => $fu_second_morning_sign_in,
                            'fu_third_morning_sign_in' => $fu_third_morning_sign_in,
                            'fu_afternoon_sign_in' => $fu_afternoon_sign_in,
                            'fu_second_afternoon_sign_in' => $fu_second_afternoon_sign_in,
                            'fu_third_afternoon_sign_in' => $fu_third_afternoon_sign_in,
                            'fu_evening_sign_in' => $fu_evening_sign_in,
                            'su_morning_sign_in' => $su_morning_sign_in,
                            'su_second_morning_sign_in' => $su_second_morning_sign_in,
                            'su_third_morning_sign_in' => $su_third_morning_sign_in,
                            'su_afternoon_sign_in' => $su_afternoon_sign_in,
                            'su_second_afternoon_sign_in' => $su_second_afternoon_sign_in,
                            'su_third_afternoon_sign_in' => $su_third_afternoon_sign_in,
                            'su_evening_sign_in' => $su_evening_sign_in,]);
    }

    public function show_add_event()
    {
        return view('add-event');
    }

    public function show_add_school()
    {
        return view('add-school');
    }

    public function show_add_attendee(){
        $events = Event::all();
        return view('add-attendee',['events' => $events]);
    }

    public function show_add_attendee_V2(){
        $schools = School::all();
        return view('add-attendeeV2' ,['schools' => $schools]);
    }

    public function show_view_events()
    {
        return view('view-event');
    }

    public function show_view_attendees()
    {
        $events = Event::all();
        $schools = School::all();
        return view('view-attendees',['events' => $events,'schools' => $schools]);
    }

    public function show_view_attendees_V2(){
        Session::put('event', $_GET['events']);
        Session::put('school', $_GET['schools']);
        return view('view-attendeesV2');
    }


    public function add_event(Request $request){
        for($x=0;$x<count($request->names);$x++){
            $event = new Event;
            $event->name = $request->names[$x];
            $event->date = Carbon::parse($request->dates[$x])->format('Y/m/d');
            if($event->save()){
                $http_response = [
                    'type' => 200,
                    'message' => 'Successfully Added Event'
                ];
            }
        }
        return json_encode($http_response);
    }

    public function add_school(Request $request){
        $exist = School::where('name',$request->name)->get();

        if($exist->count() > 0){
            $http_response = [
                'type' => 400,
                'message' => $request->name.' already exist'
            ];
        }
        else{
            $school = new School;
            $school->name = $request->name;
            if($school->save()){
                $http_response = [
                    'type' => 200,
                    'message' => 'Successfully Added '.$request->name.''
                ];
            }
        }
        return json_encode($http_response);
    }

    public function add_attendee(Request $request){
        $event = Event::where('name',$request->event)->first();
        $school = School::where('name',$request->school)->first();
        $exists = Attendee::where('first_name',$request->first_name)
                            ->where('last_name',$request->last_name)
                            ->where('event_id',$event->id)
                            ->get();

        if($exists->count() > 0){
            $http_response = [
                'type' => 400,
                'message' => 'Attendee already exist'
            ];
        }

        else{
            $attendee = new Attendee;
            $attendee->event_id = $event->id;
            $attendee->school_id = $school->id;
            $attendee->first_name = $request->first_name;
            $attendee->last_name = $request->last_name;
            $attendee->course = $request->course;
            $attendee->year = $request->year;
            $attendee->school = $request->school;
            $attendee->ID_no = $request->ID;
            if($attendee->save()){
                $http_response = [
                    'type' => 200,
                    'message' => 'Successfully Added Attendee'
                ];
            }
        }

        return json_encode($http_response);
    }

    public function View_Attendees_CSV(){
        $event = Event::where('name',Session::get('event'))->first();

        if($_GET['type'] == 'First Morning Sign In'){
            $type = 'morning_sign_in';
        }

        else if($_GET['type'] == 'Second Morning Sign In'){
            $type = 'second_morning_sign_in';
        }

        else if($_GET['type'] == 'Third Morning Sign In'){
            $type = 'third_morning_sign_in';
        }

        else if($_GET['type'] == 'First Afternoon Sign In'){
            $type = 'afternoon_sign_in';
        }

        else if($_GET['type'] == 'Second Afternoon Sign In'){
            $type = 'second_afternoon_sign_in';
        }

        else if($_GET['type'] == 'Third Afternoon Sign In'){
            $type = 'third_afternoon_sign_in';
        }

        else{
            $type = 'evening_sign_in';
        }

        if($_GET['school'] != 'All'){
            $school = School::where('name',$_GET['school'])->first();
            if($_GET['type'] != 'All'){
                $results = Attendee::where('event_id','=',$event->id)->where('school_id','=',$school->id)->where($type,'=',1)->get();
            }
            else{
                $results = Attendee::where('event_id','=',$event->id)->where('school_id','=',$school->id)->get();
            }

        }

        else{
            if($_GET['type'] != 'All'){
                $results = Attendee::where('event_id','=',$event->id)->where($type,'=',1)->get();
            }

            else{
                $results = Attendee::where('event_id','=',$event->id)->get();
            }
        }

        $dataset = [];

        foreach($results as $result){
            $dataset[] = [
                is_null($result->first_name) ? "----" : $result->first_name,
                is_null($result->last_name) ? "----" : $result->last_name,
                is_null($result->course) ? "----" :  $result->course,
                is_null($result->year) ? "----" : $result->year,
                is_null($result->school) ? "----" : $result->school,
                is_null($result->ID_no) ? "----" : $result->ID_no,
            ];
        }

        $table_headers = [
            'First Name',
            'Last Name',
            'Course',
            'Year',
            'School',
            'ID Number',
            ' ',
        ];

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"$event->name attendees.csv\";" );
        header("Content-Transfer-Encoding: binary");

        $FH = fopen('php://output', 'w');
        fputcsv($FH, $table_headers);

        foreach ($dataset as $row) {
            fputcsv($FH, $row);
        }

        exit();

    }
}
