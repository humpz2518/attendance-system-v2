<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attendee;
use App\Event;
use App\School;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Datatables;

class DataTableController extends Controller
{
    public function sign_in(Request $request){
        $now = Carbon::now('Asia/Manila');
        $attendee = Attendee::find($request->id);
        if(date("H:i", strtotime($now)) >= '00:00' && date("H:i", strtotime($now)) <= '11:59'){
            if($attendee->morning_sign_in == 0){
                $attendee->morning_sign_in = 1;
                if($attendee->save()){
                    $http_response = [
                        'type' => 200,
                        'message' => 'Successfully Signed In Mr./Ms. '.$attendee->first_name.' '.$attendee->last_name
                    ];
                }
                else{
                    $http_response = [
                        'type' => 400,
                        'message' => 'Failed to sign in'
                    ];
                }
            }
            else{
                if($attendee->second_morning_sign_in == 0){
                    $attendee->second_morning_sign_in = 1;
                    if($attendee->save()){
                        $http_response = [
                            'type' => 200,
                            'message' => 'Successfully Signed In Mr./Ms. '.$attendee->first_name.' '.$attendee->last_name
                        ];
                    }
                    else{
                        $http_response = [
                            'type' => 400,
                            'message' => 'Failed to sign in'
                        ];
                    }
                }
                else{
                    $attendee->third_morning_sign_in = 1;
                    if($attendee->save()){
                        $http_response = [
                            'type' => 200,
                            'message' => 'Successfully Signed In Mr./Ms. '.$attendee->first_name.' '.$attendee->last_name
                        ];
                    }
                    else{
                        $http_response = [
                            'type' => 400,
                            'message' => 'Failed to sign in'
                        ];
                    }
                }
            }
        }
        elseif(date("H:i", strtotime($now)) >= '12:00' && date("H:i", strtotime($now)) <= '17:59'){
            if($attendee->afternoon_sign_in == 0) {
                $attendee->afternoon_sign_in = 1;
                if ($attendee->save()) {
                    $http_response = [
                        'type' => 200,
                        'message' => 'Successfully Signed In Mr./Ms. ' . $attendee->first_name . ' ' . $attendee->last_name
                    ];
                } else {
                    $http_response = [
                        'type' => 400,
                        'message' => 'Failed to sign in'
                    ];
                }
            }
            else{
                if($attendee->second_afternoon_sign_in == 0) {
                    $attendee->second_afternoon_sign_in = 1;
                    if ($attendee->save()) {
                        $http_response = [
                            'type' => 200,
                            'message' => 'Successfully Signed In Mr./Ms. ' . $attendee->first_name . ' ' . $attendee->last_name
                        ];
                    } else {
                        $http_response = [
                            'type' => 400,
                            'message' => 'Failed to sign in'
                        ];
                    }
                }
                else{
                    $attendee->third_afternoon_sign_in = 1;
                    if ($attendee->save()) {
                        $http_response = [
                            'type' => 200,
                            'message' => 'Successfully Signed In Mr./Ms. ' . $attendee->first_name . ' ' . $attendee->last_name
                        ];
                    } else {
                        $http_response = [
                            'type' => 400,
                            'message' => 'Failed to sign in'
                        ];
                    }
                }
            }
        }
        else{
            $attendee->evening_sign_in = 1;
            if($attendee->save()){
                $http_response = [
                    'type' => 200,
                    'message' => 'Successfully Signed In Mr./Ms. '.$attendee->first_name.' '.$attendee->last_name
                ];
            }
            else{
                $http_response = [
                    'type' => 400,
                    'message' => 'Failed to sign in'
                ];
            }
        }

        return json_encode($http_response);
    }

    public function view_events_Datatable(){
        $data = Event::all();

        return Datatables::of($data)

            ->addColumn('id', function($model){
                return $model->id;
            })
            ->addColumn('name', function($model){
                return $model->name;
            })
            ->addColumn('date', function($model){
                return date(' F j, Y',strtotime($model->date));
            })
            ->make(true);
    }

    public function view_attendees_Datatable(Request $request){
        $event = Event::where('name',$request->event)->first();
        if($request->school != 'All'){
            $school = School::where('name',$request->school)->first();
            $data = Attendee::where('event_id','=',$event->id)->where('school_id','=',$school->id)->select('id', 'first_name', 'last_name', 'course', 'year', 'school', 'morning_sign_in','second_morning_sign_in','third_morning_sign_in','afternoon_sign_in','second_afternoon_sign_in','third_afternoon_sign_in','evening_sign_in')->get();
        }

        else{
            $data = Attendee::where('event_id','=',$event->id)->select('id', 'first_name', 'last_name', 'course', 'year', 'school', 'morning_sign_in','second_morning_sign_in','third_morning_sign_in','afternoon_sign_in','second_afternoon_sign_in','third_afternoon_sign_in','evening_sign_in')->get();
        }

        return Datatables::of($data)
            ->rawColumns(['actions'])
            ->addColumn('id', function($model){
                return $model->id;
            })
            ->addColumn('first_name', function($model){
                return $model->first_name;
            })
            ->addColumn('last_name', function($model){
                return $model->last_name;
            })
            ->addColumn('course', function($model){
                return $model->course;
            })
            ->addColumn('year', function($model){
                return $model->year;
            })
            ->addColumn('school', function($model){
                return $model->school;
            })
            ->addColumn('morning_sign_in', function($model){
                if($model->morning_sign_in == 0){
                    return 'No';
                }
                else{
                    return 'Yes';
                }
            })
            ->addColumn('second_morning_sign_in', function($model){
                if($model->second_morning_sign_in == 0){
                    return 'No';
                }
                else{
                    return 'Yes';
                }
            })
            ->addColumn('third_morning_sign_in', function($model){
                if($model->third_morning_sign_in == 0){
                    return 'No';
                }
                else{
                    return 'Yes';
                }
            })
            ->addColumn('afternoon_sign_in', function($model){
                if($model->afternoon_sign_in == 0){
                    return 'No';
                }
                else{
                    return 'Yes';
                }
            })
            ->addColumn('second_afternoon_sign_in', function($model){
                if($model->second_afternoon_sign_in == 0){
                    return 'No';
                }
                else{
                    return 'Yes';
                }
            })
            ->addColumn('third_afternoon_sign_in', function($model){
                if($model->third_afternoon_sign_in == 0){
                    return 'No';
                }
                else{
                    return 'Yes';
                }
            })
            ->addColumn('evening_sign_in', function($model){
                if($model->evening_sign_in == 0){
                    return 'No';
                }
                else{
                    return 'Yes';
                }
            })
            ->addColumn('actions', function($model){
                $actions = '<button type="button"  data-value="' . $model->id . '" class="btn btn-primary attendee" data-route="'. route('sign-in') . '">Sign In</button>';
                return $actions;
            })
            ->make(true);
    }
}
